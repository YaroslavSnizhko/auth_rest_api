<?php

namespace App\Entity;

/**
* @property int $id;
* @property string $userId;
* @property string $sourceLabel;
* @property string $dateCreated;
 */
class Tracking
{
    private int $id;

    private string $userId;

    private string $sourceLabel;

    private string $dateCreated;

    /**
     * @return int $id
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $countRecords
     * @return $this
     */
    public function setId(int $countRecords = 0): self
    {
        $this->id = $this->idCounter($countRecords);

        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSourceLabel(): ?string
    {
        return $this->sourceLabel;
    }

    /**
     * @param string $sourceLabel
     * @return $this
     */
    public function setSourceLabel(string $sourceLabel): self
    {
        $this->sourceLabel = $sourceLabel;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDateCreated(): ?string
    {
        return $this->dateCreated;
    }

    /**
     * @param string $date
     * @return $this
     */
    public function setDateCreated(string $date): self
    {
        $this->dateCreated = $date;

        return $this;
    }

    /**
     * @param int $number
     * @return int
     */
    public function idCounter(int $number)
    {
        $this->id = $number + 1;

        return $this->id;
    }

    /**
     * @return array
     */
    public function getDataArray(): array
    {
        return [
            'id' => $this->id,
            'user_id' => $this->userId,
            'source_label' => $this->sourceLabel,
            'date_created' => $this->dateCreated
        ];
    }
}
