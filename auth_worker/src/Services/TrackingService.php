<?php

namespace App\Services;

use App\Entity\Tracking;
use App\Validation\JsonValidator;
use SocialTech\StorageInterface;

class TrackingService
{
    private string $file = 'UserAction';

    private string $type = 'json';

    private string $path;

    private array $data = [];

    /**
     * @var StorageInterface
     */
    private StorageInterface $storage;
    /**
     * @var Tracking
     */
    private Tracking $analytic;

    public function __construct(Tracking $analytic, StorageInterface $storage)
    {
        $this->analytic = $analytic;
        $this->storage = $storage;
        $this->path = "{$_ENV['RESOURCES_FOLDER']}{$this->file}.{$this->type}";
    }

    /**
     * create or update json file with tracking data
     * @param string $json
     */
    public function create(string $json): void
    {
        JsonValidator::isJson($json);

        $dataObject = json_decode($json);

        $this->analytic->setUserId($dataObject->userId);
        $this->analytic->setSourceLabel($dataObject->sourceLabel);
        $this->analytic->setDateCreated($dataObject->dateCreated);

        if($this->storage->exists($this->path)) {
            $this->data = json_decode($this->storage->load($this->path), true);
            $this->analytic->setId(count($this->data));
        } else {
            $this->analytic->setId();
        }

        $this->addData($this->analytic->getDataArray());

        $this->storage->store($this->path, json_encode($this->data, JSON_PRETTY_PRINT));
    }

    /**
     * @param array $data
     * @return $this
     */
    private function addData(array $data): self
    {
        array_push($this->data, $data);

        return $this;
    }

}