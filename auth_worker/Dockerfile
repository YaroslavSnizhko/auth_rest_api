FROM php:fpm

# Set the WORKDIR to /app so all following commands run in /app
WORKDIR /app

RUN apt-get update && apt-get install -y  --no-install-recommends librabbitmq-dev

RUN pecl install amqp

RUN docker-php-ext-enable amqp

RUN sed -i "s/\(user\|group\) = www-data/\1 = root/" /usr/local/etc/php-fpm.d/www.conf

RUN docker-php-ext-install sockets

RUN docker-php-ext-enable sockets

RUN apt-get update && apt-get install -y git

RUN apt-get update && apt-get install -y wget

ENV DOCKERIZE_VERSION v0.6.1
RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer

# Copy composer files into the app directory.
COPY composer.json ./

# Install dependencies with Composer.
# --no-interaction makes sure composer can run fully automated
RUN composer install

COPY . .

CMD ["php-fpm", "-R"]
