<?php

namespace App\DataTransform;

class ObjectToArrayTransform implements ObjectTransformInterface
{

    public array $data;

    /**
     * @param object $data
     * @return $this
     */
    public function transform(object $data): self
    {
        try {
            $reflection = new \ReflectionClass($data);
            $props = $reflection->getProperties();
            foreach ($props as $prop) {
                $prop->setAccessible(true);
                $this->data[$prop->getName()] = $prop->getValue($data);
            }

            return $this;
        } catch (\ReflectionException $e) {
            throw new \DomainException($e);
        }
    }

}