<?php


namespace App\DataTransform;


interface ObjectTransformInterface
{
    public function transform(object $data);
}