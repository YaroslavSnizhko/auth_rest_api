<?php
namespace App\Services;

use App\Entity\User;
use \App\Repository\UserRepository;
use App\Dto\UserDto;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class UserService
{
    /**
     * @var UserRepository
     */
    private UserRepository $userRepository;
    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $passwordEncoder;

    public function __construct(UserRepository $userRepository, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param UserDto $userDto
     */
    public function registrationUser(UserDto $userDto): void
    {
        $this->userNameIsUnique($userDto->nickname);

        $user = new User();

        $usersCount = count($this->userRepository->getAll());
        $passwordEncoded = $this->passwordEncoder->encodePassword($user, $userDto->password);

        $user->setId($usersCount);
        $user->setNickname($userDto->nickname);
        $user->setFirstname($userDto->firstname);
        $user->setLastname($userDto->lastname);
        $user->setAge($userDto->age);
        $user->setPassword($passwordEncoded);
        $user->setCreatedAt();
        $user->setUpdatedAt();

        $this->userRepository->add($user);
        $this->userRepository->save();
    }

    /**
     * @param $nickname
     */
    private function userNameIsUnique($nickname): void
    {
        if (!empty($this->userRepository->findByNickname($nickname))) {
            throw new \RuntimeException('Username already exists');
        }
    }
}