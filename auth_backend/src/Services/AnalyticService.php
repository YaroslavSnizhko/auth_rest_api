<?php

namespace App\Services;

use App\DataTransform\ObjectToArrayTransform;
use App\DataTransform\ObjectTransformInterface;
use App\Entity\Analytic;
use App\Message\ActionMessage;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;

class AnalyticService
{

    /**
     * @var Request
     */
    private Request $request;
    /**
     * @var MessageBusInterface
     */
    private MessageBusInterface $bus;

    /**
     * @var mixed $data
     */
    private $data;
    /**
     * @var ObjectTransformInterface
     */
    private ObjectTransformInterface $objectTransform;

    public function __construct(Request $request, MessageBusInterface $bus, ObjectTransformInterface $objectTransform)
    {
        $this->request = $request;
        $this->bus = $bus;
        $this->objectTransform = $objectTransform;
    }

    public function sendToQueue(): void
    {
        $data = $this->objectTransform->transform($this->data)->data;

        $this->bus->dispatch(new ActionMessage(json_encode($data)));
    }

    /**
     * @param string $sourceLabel
     * @return $this
     */
    public function add(string $sourceLabel): self
    {
        $model = new Analytic();

        $model->setUserId(ip2long($this->request->getClientIp()));
        $model->setSourceLabel($sourceLabel);
        $model->setDateCreated();

        $this->data = $model;

        return $this;
    }
}