<?php

namespace App\Storage;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class JsonFileStorage implements JsonFileStorageInterface
{
    /**
     * @var string
     */
    private string $basePath;
    /**
     * @var string
     */
    private string $absolutePath;
    /**
     * @var array
     */
    private array $data;

    const TYPE = 'json';

    /**
     * @var Filesystem
     */
    private Filesystem $fileSystem;
    /**
     * @var Finder
     */
    private Finder $finder;
    /**
     * @var string
     */
    private string $fileName;

    public function __construct()
    {
        $this->basePath =  $_ENV['RESOURCES_FOLDER'];;
        $this->fileSystem = new Filesystem();
        $this->finder = new Finder();
    }

    /**
     * @param string $name
     * @return self
     */
    public function readFile(string $name): self
    {
        $files = $this->finder->files()->name("{$name}." . self::TYPE)->in($this->basePath);

        foreach ($files as $file) {
            $this->absolutePath = $file->getRealPath();
            $this->data = json_decode($file->getContents(), true);
        }

        return $this;
    }

    public function saveFile(): void
    {
        try {
            $this->fileSystem->dumpFile($this->absolutePath, json_encode($this->data, JSON_PRETTY_PRINT));
        } catch (\Exception $e) {
            throw new \RuntimeException($e);
        }
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return $this
     */
    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @param array $data
     * @return $this
     */
    public function addData(array $data): self
    {
        array_push($this->data, $data);

        return $this;
    }
}