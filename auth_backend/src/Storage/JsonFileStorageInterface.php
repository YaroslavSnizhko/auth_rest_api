<?php

namespace App\Storage;

interface JsonFileStorageInterface
{
    public function readFile(string $name);

    public function saveFile();

    public function getData();

    public function setData(array $data);

    public function addData(array $data);
}