<?php


namespace App\DTO;


class AnalyticDto
{
    public int $userId;

    public string $sourceLabel;

    public \DateTime $dataCreated;
}