<?php
namespace App\DTO;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

final class UserDto
{
    /**
     * @Type("string")
    **/
    public string $nickname;

    /**
     * @Type("string")
     **/
    public string $firstname;

    /**
     * @Type("string")
     **/
    public string $lastname;

    /**
     * @Type("int")
     **/
    public int $age;

    /**
     * @Type("string")
     **/
    public string $password;

    public function __construct(string $nickname)
    {
        $this->nickname = $nickname;
    }
}