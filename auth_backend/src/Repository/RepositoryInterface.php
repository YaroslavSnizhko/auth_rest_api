<?php


namespace App\Repository;


interface RepositoryInterface
{
    public function get(int $id);

    public function add(object $data);

    public function save();

    public function remove(string $id);
}