<?php

namespace App\Repository;

use App\DataTransform\ObjectTransformInterface;
use App\Entity\User;
use App\Storage\JsonFileStorageInterface;
use PHPUnit\Exception;

class UserRepository implements RepositoryInterface
{
    private string $document = 'user';
    /**
     * @var User
     */
    private User $user;
    /**
     * @var ObjectTransformInterface
     */
    private ObjectTransformInterface $objectTransform;
    /**
     * @var JsonFileStorageInterface
     */
    private JsonFileStorageInterface $jsonFileStorage;


    public function __construct(User $user, ObjectTransformInterface $objectTransform, JsonFileStorageInterface $jsonFileStorage)
    {

        $this->user = $user;
        $this->objectTransform = $objectTransform;
        $this->jsonFileStorage = $jsonFileStorage;
    }

    /**
     * @param int $id
     * @return User|null
     */
    public function get(int $id): ?User
    {
        $data = $this->jsonFileStorage->readFile($this->document)->getData();

        foreach ($data as $key => $user) {
            if (in_array($id, $user)) {
                $this->user->setAll($user);

                return $this->user;
            }
        }

        return null;
    }


    /**
     * @param string $nickname
     * @return User|null
     */
    public function findByNickname(string $nickname): ?User
    {
        $data = $this->jsonFileStorage->readFile($this->document)->getData();

        foreach ($data as $key => $user) {
            if (in_array($nickname, $user)) {
                $this->user->setAll($user);

                return $this->user;
            }
        }

        return null;
    }

    /**
     * @param object $user
     */
    public function add(object $user): void
    {
        $this->objectTransform->transform($user);

        $data = $this->objectTransform->data;

        $this->jsonFileStorage->addData($data);
    }

    public function save(): void
    {
        try {
            $this->jsonFileStorage->saveFile();
        } catch (Exception $e) {
            throw new $e;
        }
    }

    /**
     * @param string $id
     * @return bool
     */
    public function remove($id): bool
    {
        $data = $this->jsonFileStorage->getData();

        foreach ($data as $key => $user) {
            if (in_array($id, $user)) {
                unset($data[$key]);
                $this->jsonFileStorage->setData($data);
                $this->jsonFileStorage->saveFile();

                return true;
            }
        }

        return false;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->jsonFileStorage->readFile($this->document)->getData();
    }
}
