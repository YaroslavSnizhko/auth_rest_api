<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @param int $userId
 * @param string $sourceLabel
 * @param string $dateCreated
 */
class Analytic
{

    private $userId;

    private $sourceLabel;

    private $dateCreated;


    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSourceLabel(): ?string
    {
        return $this->sourceLabel;
    }

    /**
     * @param string $sourceLabel
     * @return $this
     */
    public function setSourceLabel(string $sourceLabel): self
    {
        $this->sourceLabel = $sourceLabel;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDateCreated(): ?\DateTimeInterface
    {
        return $this->dateCreated;
    }

    /**
     * @return $this
     */
    public function setDateCreated(): self
    {
        $this->dateCreated = date("Y-m-d H:i:s");

        return $this;
    }
}
