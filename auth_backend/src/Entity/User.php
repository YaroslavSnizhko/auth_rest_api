<?php

namespace App\Entity;

use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @property int $id
 * @property string $nickname
 * @property array $roles
 * @property string $password
 * @property string $firstname
 * @property string $lastname
 * @property int $age
 * @property string $created_at
 * @property string $updated_at
 */

class User implements UserInterface
{
    private int $id;

    private ?string $nickname;

    private array $roles = [];

    private string $password;

    private string $firstname;

    private string $lastname;

    private int $age;

    private string $created_at;

    private string $updated_at;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $countRecords
     * @return $this
     */
    public function setId(int $countRecords = 0): self
    {
        $this->id = $this->idCounter($countRecords);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    /**
     * @param string $nickname
     * @return $this
     */
    public function setNickname(string $nickname): self
    {
        $this->nickname = $nickname;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->nickname;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * @param array $roles
     * @return $this
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return string|null
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     * @return $this
     */
    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     * @return $this
     */
    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getAge(): ?int
    {
        return $this->age;
    }

    /**
     * @param int $age
     * @return $this
     */
    public function setAge(int $age): self
    {
        $this->age = $age;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->created_at;
    }

    /**
     * @ORM\PrePersist
     * @param \DateTimeInterface $created_at
     * @return User
     */
    public function setCreatedAt(): self
    {
        $this->created_at = date('Y-m-d h:i:s');

        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return $this->updated_at;
    }

    /**
     * @ORM\PrePersist
     * @param \DateTimeInterface $updated_at
     * @return User
     */
    public function setUpdatedAt(): self
    {
        $this->updated_at = date('Y-m-d h:i:s');

        return $this;
    }

    /**
     * @param array $user
     * @return $this
     */
    public function setAll(array $user): self
    {
        $this->id = $user['id'];
        $this->firstname = $user['firstname'];
        $this->lastname = $user['lastname'];
        $this->age = $user['age'];
        $this->password = $user['password'];
        $this->created_at = $user['created_at'];
        $this->updated_at = $user['updated_at'];

        return $this;
    }

    /**
     * @param int $number
     * @return int
     */
    public function idCounter(int $number)
    {
        $this->id = $number + 1;

        return $this->id;
    }
}
