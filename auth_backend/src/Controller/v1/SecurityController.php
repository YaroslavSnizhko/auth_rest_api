<?php

namespace App\Controller\v1;

use App\DataTransform\ObjectToArrayTransform;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Security\LoginAuthenticator;
use App\Storage\JsonFileStorage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\DTO\UserDto;
use App\Services\AnalyticService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{
    /**
     * @var AnalyticService
     */
    private AnalyticService $analyticService;

    public function __construct(RequestStack $requestStack, MessageBusInterface $bus)
    {
        $this->analyticService = new AnalyticService($requestStack->getCurrentRequest(), $bus, new ObjectToArrayTransform());
    }

    /**
     * @Rest\Post("/login", name="login")
     * @ParamConverter("userDto", class="App\DTO\UserDto", converter="fos_rest.request_body")
     * @param UserDto $userDto
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function postLoginAction(UserDto $userDto, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        // send data to analytic service
        $this->analyticService->add('login');
        $this->analyticService->sendToQueue();

        $userRepository = new UserRepository(
            new User(),
            new ObjectToArrayTransform(),
            new JsonFileStorage()
        );

        $security = new LoginAuthenticator($passwordEncoder, $userRepository);

        $security->authentication($userDto);

        return $this->json([
            "access_token" => "token test123 only example"
        ], Response::HTTP_OK);
    }
}
