<?php

namespace App\Controller\v1;

use App\DataTransform\ObjectToArrayTransform;
use App\DTO\UserDto;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Services\AnalyticService;
use App\Storage\JsonFileStorage;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use App\Services\UserService;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends AbstractFOSRestController
{

    /**
     * @var AnalyticService
     */
    private AnalyticService $analyticService;

    public function __construct(RequestStack $requestStack, MessageBusInterface $bus)
    {
        $this->analyticService = new AnalyticService($requestStack->getCurrentRequest(), $bus, new ObjectToArrayTransform());
    }

    /**
     * @Rest\Post("/registration", name="registration")
     * @ParamConverter("userDto", class="App\DTO\UserDto", converter="fos_rest.request_body")
     * @param UserDto $userDto
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    public function postRegisterAction(UserDto $userDto, UserPasswordEncoderInterface $encoder): Response
    {
        // send data to analytic service
        $this->analyticService->add('registration');
        $this->analyticService->sendToQueue();

        $userRepository = new UserRepository(
            new User(),
            new ObjectToArrayTransform(),
            new JsonFileStorage()
        );

        $userService = new UserService($userRepository, $encoder);

        $userService->registrationUser($userDto);

        return $this->json($userDto, Response::HTTP_CREATED);
    }
}
