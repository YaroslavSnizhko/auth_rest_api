<?php
namespace App\Security;

use App\DTO\UserDto;
use App\Entity\User;
use App\Repository\RepositoryInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;

class LoginAuthenticator
{

    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $passwordEncoder;
    /**
     * @var RepositoryInterface
     */
    private RepositoryInterface $userRepository;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, RepositoryInterface $userRepository)
    {

        $this->passwordEncoder = $passwordEncoder;
        $this->userRepository = $userRepository;
    }

    /**
     * @param $nickname
     * @return User
     */
    public function getUser(string $nickname): User
    {
        $user = $this->userRepository->findByNickname($nickname);

        if (!$user) {
            // fail authentication with a custom error
            throw new CustomUserMessageAuthenticationException('Nickname could not be found.');
        }

        return $user;
    }

    /**
     * @param $password
     * @param UserInterface $user
     * @return bool
     */
    public function checkPassword($password, UserInterface $user): bool
    {
        if(!$this->passwordEncoder->isPasswordValid($user, $password))
        {
            throw new \RuntimeException('Password is not valid');
        }
        return true;
    }

    /**
     * @param UserDto $userDto
     * */
    public function authentication(UserDto $userDto): User
    {
        $user = $this->getUser($userDto->nickname);

        $this->checkPassword($userDto->password, $user);

        return $user;
    }

}