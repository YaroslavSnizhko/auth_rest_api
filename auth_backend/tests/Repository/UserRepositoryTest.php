<?php

namespace App\tests;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Storage\JsonFileStorage;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

class UserRepositoryTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $_ENV['RESOURCES_FOLDER'] = __DIR__ . '/../../src/Resources';
    }

    public function testAdd(): void
    {
        $user = new User();
        $userRepository = new UserRepository();

        $usersCount = count($userRepository->getAll());

        $user->setId($usersCount);
        $user->setNickname('test');
        $user->setFirstname('test');
        $user->setLastname('test');
        $user->setAge('10');
        $user->setPassword('test');
        $user->setCreatedAt();
        $user->setUpdatedAt();

        $userRepository->add($user);
        $userRepository->save();

        $found = $userRepository->findByNickname('test');

        $this->assertEquals($user->getFirstname(), $found->getFirstname());
        $this->assertEquals($user->getLastname(), $found->getLastname());
        $this->assertEquals($user->getAge(), $found->getAge());
        $this->assertEquals($user->getCreatedAt(), $found->getCreatedAt());
        $this->assertEquals($user->getUpdatedAt(), $found->getUpdatedAt());

    }

    public function testFindByNickName()
    {
        $userRepository = new UserRepository();

        $found = $userRepository->findByNickname('test');

        $this->assertEquals('test', $found->getFirstname());
    }

    public function testRemove(): void
    {
        $userRepository = new UserRepository();

        $getUser = $userRepository->findByNickname('test');

        $userRepository->remove($getUser->getId());

        $found = $userRepository->get($getUser->getId());

        $this->assertEquals(NULL, $found);

    }

    public function testSave(): void
    {
        $user = new User();
        $userRepository = new UserRepository();

        $usersCount = count($userRepository->getAll());

        $user->setId($usersCount);
        $user->setNickname('test');
        $user->setFirstname('test');
        $user->setLastname('test');
        $user->setAge('10');
        $user->setPassword('test');
        $user->setCreatedAt();
        $user->setUpdatedAt();

        $userRepository->add($user);
        $userRepository->save();

        $found = $userRepository->findByNickname('test');

        $this->assertEquals($user->getFirstname(), $found->getFirstname());
        $this->assertEquals($user->getLastname(), $found->getLastname());
        $this->assertEquals($user->getAge(), $found->getAge());
        $this->assertEquals($user->getCreatedAt(), $found->getCreatedAt());
        $this->assertEquals($user->getUpdatedAt(), $found->getUpdatedAt());

        $userRepository->remove($user->getId());

    }

}