<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class RegistrationControllerTest extends WebTestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $_ENV['RESOURCES_FOLDER'] = __DIR__ . '/../../src/Resources';
    }

    public function testPostRegister(): void
    {
        $client = static::createClient();

        $client->request(
            'POST',
            '/v1/registration',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            '{"nickname":"test123","firstname": "user_test","lastname": "user_test","age": 20,"password": "user_test"}'
        );

        $response = json_decode($client->getResponse()->getContent());

        $this->assertEquals($response->nickname, 'test123');
    }
}