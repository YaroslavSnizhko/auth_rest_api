<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class SecurityControllerTest extends WebTestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $_ENV['RESOURCES_FOLDER'] = __DIR__ . '/../../src/Resources';
    }

    public function testPostLoginAction(): void
    {
        $client = static::createClient();

        $client->request(
            'POST',
            '/v1/login',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            '{"nickname":"test123","firstname": "user_test","lastname": "user_test","age": 20,"password": "user_test"}'
        );

        $response = json_decode($client->getResponse()->getContent());

        $this->assertTrue(isset($response->access_token), 'User login');
    }
}