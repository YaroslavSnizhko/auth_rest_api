# auth-service
Приложение сотоит из 2 частей: auth_backend, auth_worker.Для основы auth_backend приложения используется Symfony5. 
Для auth_worker используется базовый каркас тестового задания. Входящие запросы по  
http://localhost:8000/v1/registration, http://localhost:8000/v1/login обрабатывает сервис auth_backend. 
Данные auth_backend хранятся в "/auth_backend/src/Resources" в формате JSON, а auth_worker в /auth_worker/storage. Действия регистрации и аутентификации так же добавляются в очередь "messages"(это базовая в symfony) в rabbitMq
# how to start
```bash
docker-compose up -d
```

### Install vendors
```bash
cd auth_backend && composer install
cd auth_worker && composer install
```


### Rebuild images & recreate containers
```bash
docker-compose build --no-cache
docker-compose up -d --force-recreate
```

### registration request
send request to http://localhost:8000/v1/registration   
'Content-Type' : 'application/json'
```json
{
    "nickname": "test",
    "password": "test",
    "firstname": "test",
    "lastname": "test",
    "age": 20
}
	
```

### login request
send request to http://localhost:8000/v1/login  
'Content-Type' : 'application/json'
```json
{
    "nickname": "test",
    "password": "test"
}
	
```

### Data Storage

```json
{
    "auth_backend": "/auth_backend/src/storage",
    "auth_worker": "/auth_worker/storage"
}
	
```

### RabbitMQ management
[http://localhost:15672/](http://localhost:15672/) **[guest:guest]**

